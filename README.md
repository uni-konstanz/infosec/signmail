# smime-tools

```
Copyright (c) 2018-2020 thomas.zink _at_ uni-konstanz _dot_ de
Usage of the works is permitted provided that this instrument is retained with the works, so that     any entity that uses the works is notified of this instrument.
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
```

A toolbox of scripts to process and convert SMIME certificates, including
sending signed and encrypted emails.

## Content

### cert2ssh

Extract SSH private and public keys from S/MIME PKCS12 certificate.

### cert-convert

Convert certificates to and from different formats

### remove-cert-pw

remove passwort from smime certificate

### smail

Send signed and encrypted mails using `openssl` and `sendmail`.

This script uses `openssl` to sign and encrypt emails with a specified smime certificate.
It then uses sendmail to send the signed email.

The script is especially useful to script mass sending of signed emails.
In this case, it is useful to provide the certificate without password.

The content type heder with charset must be set within the email. E.g:

```
Content-Type: text/plain; charset=UTF-8
```

## Requirements

* `openssl` to sign and encrypt the mail
* `sendmail` and a configured mail transfer agent to send the mail. e.g. `msmtp`, `opensmtpd`, `postfix`, `exim4`

## Usage

```
usage: smail -c <signer_certificate> <-t <to_address> | -e <recipient_certificate>> -m <mail_infile> -s [subject]

  	-c <signer_certificate>: smime certificate used to sign the email
  	-t <to_address>:         recipients email address
  	-s <subject>:            email subject
  	-m <mail_infile>:        file with the email to sign
```

Example:

```
# smail -c ~/certs/mycert.pem -t admin@example.com -m examplemail.txt -s "signmailtest"
```