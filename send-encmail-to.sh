#!/bin/env bash

# check version
#ossl=openssl-1.1
#ossl=openssl-1.0
ossl=openssl

usage() {
    echo "usage: $(basename "$0") <path/to/gpg-encrypted-sender-key> <path/to/recipient.crt> <path/to/mail> [subject]"
}

if [ ! -f "${1}" ] || [ ! -f "${2}" ] || [ ! -f "${3}" ]; then 
    usage;
    exit 0;
fi

encpem="$1"
recipient="$2"
mail="$3"
subject=""

# encrypted signer with temp (fifo does not work with ossl smime, blocks)
tmpdir=$(mktemp -d --tmpdir="$XDG_RUNTIME_DIR")
signer=$(mktemp --tmpdir="$tmpdir")
mailsigned=$(mktemp --tmpdir="$tmpdir")
mailencred=$(mktemp --tmpdir="$tmpdir")
>&2 echo "${signer} ${mailsigned} ${mailencred}"

#mkfifo -m "${signer}" 
#gpg -d "${encpem}" >"${signer}"&
gpg -d "${encpem}" >"${signer}"

# extract from: address from signer cert
from="$(${ossl} x509 -text -in ${signer} | grep email: | sed 's/email://' | tr -d ' ')"
>&2 echo "from: ${from}"

# extract to: address from recipient cert
to="$(${ossl} x509 -text -in "${recipient}" | grep email: | sed 's/email://' | tr -d ' ')"
>&2 echo "to: ${to}"

# set subject
if [ -n "$4" ]; then subject="$4"; fi
>&2 echo "subject: ${subject}"

# sign mail
${ossl} smime -sign -in "${mail}" -signer ${signer} -from "${from}" -to "${to}" -subject "${subject}" > "${mailsigned}" 
[ -f "${mailsigned}" ] &&  >&2 echo "mail signed"

# encrypt mail
cat "${mailsigned}"| ${ossl} smime -encrypt -from "${from}" -to "${to}" -subject "${subject}" "${recipient}" > "${mailencred}"
[ -f "${mailencred}" ] && >&2 echo "mail encrypted"

# send mail
cat "${mailencred}" | sendmail -to "${to}" && >&2 echo "mail sent"

# cleanup
rm -rv "${tmpdir}" >&2
